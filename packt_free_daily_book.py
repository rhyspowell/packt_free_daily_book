import boto3
import os
import scrapy
import time
import uuid

from notifiers import get_notifier


# Check required os environs
if 'AWS_ACCESS_KEY' not in os.environ:
    print('AWS_ACCESS_KEY is not set')
    exit(1)
else:
    AWS_ACCESS_KEY = os.environ['AWS_ACCESS_KEY']

if 'AWS_SECRET_ACCESS_KEY' not in os.environ:
    print('AWS_SECRET_ACCESS_KEY is not set')
    exit(1)
else:
    AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']

if 'BUCKET' not in os.environ:
    print('Bucket name is missing')
    exit(1)
else:
    BUCKET = os.environ['BUCKET']

if 'HOOK' not in os.environ:
    print('HOOK is not set')
    exit(1)
else:
    HOOK = os.environ['HOOK']


class PacktSpider(scrapy.Spider):
    handle_httpstatus_list = [401, 403, 404, 408, 500, 502, 503, 504]
    name = 'packt'

    def start_requests(self):
        urls = ['https://www.packtpub.com/packt/offers/free-learning']
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        slack = get_notifier('slack')
        count = 0
        while True:
            if response.status == 200:
                break
            elif response.status == 404:
                slack.notify(webhook_url=HOOK, message='404 URL is not good')
                exit(1)
            elif response.status == 401 or response.status == 403:
                slack.notify(
                    webhook_url=HOOK,
                    message='We might have been blocked status '
                    + str(response.status)
                    )
                exit(1)
            else:
                slack.notify(
                    webhook_url=HOOK,
                    message='Warning connection type errors. Error number '
                    + str(response.status)
                    + ' count '
                    + str(count)
                    )
                time.sleep(300)
                count += 1
                if count == 4:
                    exit(1)
                else:
                    self.start_requests()

        title = response.xpath(
            'normalize-space(//div[@class="dotd-title"])'
            ).extract()[0]

        slack.notify(webhook_url=HOOK, message='The free title from packt today is ' + title)
        if 'PyBiteshook' in os.environ:
            slack.notify(webhook_url=os.environ['PyBiteshook'],
                         message='The free title from packt today is '
                         + title +
                         '\nCheck https://www.packtpub.com/packt/offers/free-learning')
        print(title)

        file_text = f''' {{
  "uid": "urn:uuid:{uuid.uuid4()}",
  "updateDate": "{time.strftime("%Y-%m-%dT%H:%M:%SZ")}",
  "titleText": "Packt free learning ebook for today",
  "mainText": "{title}.",
  "redirectionUrl": "https://www.packtpub.com/packt/offers/free-learning"
}}'''

        s3_client = boto3.client(
            's3',
            aws_access_key_id=AWS_ACCESS_KEY,
            aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
            region_name='us-east-1'
        )
        with open('feed.json', 'w') as f:
            f.write(file_text)

        # Upload the file to S3
        response = s3_client.put_object(
            ACL='public-read',
            Bucket=BUCKET,
            Key='feed.json',
            Body=file_text,
            ContentEncoding='utf-8',
            ContentType='application/json',
            StorageClass='REDUCED_REDUNDANCY'
            )
        print(response)
